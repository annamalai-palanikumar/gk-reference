1) What are the five levels of DevOps practice?
  values, principles, methods, practices, and tools
2) Information Technology Infrastructure Library or ITIL provides a comprehensive _____ for designing, managing, and controlling IT processes.
  process-model based approach
3) Suppose you and your project manager are interested in the infrastructure as code approach. What is the chief issue that your team may face when utilizing the infrastructure as code approach?
  The mindset and habits of your team members.
4) Which of the following explains the concept of blue-green deployment?
  There are two identical production environments in which one is live (Blue) and the other is idle (Green). When new software is fully tested and deployed to Green, the router switches traffic from Blue to Green
5) What are the four values in the CAMS model?
  CAMS: Culture, Automation, Measurement, and Sharing
6) Certain companies utilize immutable deployment, in which changes to the system are _____ as opposed to _____.
  replaced; updated
7) Which of the following explains the concept of containers?
  Containers are stand-alone software packages that contain runtime components to function independently.
8) _____ describe when software is deployed quickly into production since the team members make sure the application goes through complete automated testing.
  Continuous deployments
9) How can you implement experimentation and learning in your work environment?
  Rotate your employees to attend conferences and share vital information with their colleagues.
10) Tools are launched daily for diverse purposes. Which qualities should you look for in a tool before combining it into a toolchain?
  All of these answers
11) Which manifesto, created in 2010 by security professionals, endorses the call for secure coding or programming?
  The Rugged Manifesto
12) DevOpsDays, DevOps Enterprise Summit, and Velocity. are examples of _____.
  Conferences
13) Let’s say your colleague is unable to attend a DevOps conference. How would you recommend she learn more about DevOps concepts?
  Introduce her to books and articles devoted to DevOps.
14) Let’s say you want to employ the Kaizen cultural practice in your company. What are the four main factors in the Kaizen cycle?
  plan, do, check, and act
15) Which leading social media network should an individual engage in to keep abreast of the DevOps news and development?
  Twitter
16) What computing approach creates systems in the cloud instead of depending on servers?
  Serverless architecture
17) Suppose your company is weighing options on implementing private or public cloud. How would you explain the difference between public or private cloud?
  Private cloud is internally created, built, and operated by your own company. Public cloud is created by an individual or organization, such as Amazon Web Services, and they allow users from around the world to use their services.
18) Let’s say your colleague wants to know more about the central tenet of Lean. How would you explain it to them?
  The objective of lean is to ensure that value stream reaches the customer through products and services while eliminating waste.
19) What is a build log?
  It is a record of all the tests that were run along with their results.
20) When considering the People over Process over Tools methodology, why is it important to focus on People first?
  You need a responsible individual with the appropriate skill set who knows the planning and implementation of the project and tools otherwise there will be wastage and inferior product and solution delivery.
21) _____ refers to the average time your service recovers and restores services from disruptions, such as an outage.
  Mean time to recovery
22) Developers can find bottlenecks during the development process by using a _____ instead of employing the “black box” methodology.
  code profiler
23) Let’s say your colleagues are debating whether to employ synthetic or real user monitoring. How is real user monitoring different than synthetic checks?
  Real user monitoring records all the actual end user’s engagement with the web application. Synthetic checks simulate how a user might interact with the application
24) Suppose your Dev team has frequent issues with an Ops Teams in your company. Blame is often tossed around between both groups. How would you solve this issue?
  Embed Ops engineers in your development teams, assign both teams to be in one chat room, and allow them to read each other's source code.
25) _____ places all log records in a secured storage area or location.
  Centralized logging
26) Artifacts should be _____.
  built once and deployed as needed
27) Let’s say your company’s server went down for a few days due to a hurricane. How would having a status page affect your company’s reputation throughout the outage?
  Communicating on the status page throughout the outage will notify the users of the issues and deadline for resolution. It can improve the trust and satisfaction of the customers.
28) Suppose some of your tests are slow. Which procedures should you select to handle a slow test?
  all of these answers
29) Suppose you want to test the resilience of your company’s e-commerce website in preparation for the holiday shopping season. How would having a Chaos Monkey test the abilities of your servers?
  The Chaos Monkey will interrupt normal operations for your server, forcing your engineers to find methods to make the server robust and tolerant to instance failures.
30) Jenkins is an example of _____.
  a continuous integration tool
31) DevOps supports the elimination of _____ because it can hamper collaboration, operations and morale within the company.
  silos
